import { TestBed, inject } from '@angular/core/testing';

import { FavoritesServiceService } from './favorites-service.service';

describe('FavoritesServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FavoritesServiceService]
    });
  });

  it('should be created', inject([FavoritesServiceService], (service: FavoritesServiceService) => {
    expect(service).toBeTruthy();
  }));
});
