import { element } from 'protractor';
import { Person } from './../classes/person';
import { Joke } from './../classes/joke';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class JokeServiceService {

  // API Documentation in http://www.icndb.com/api/
  BASE_SERVICE_URL: string = "http://api.icndb.com";
  CUSTOM_SERVICE_URL_FIRST: string = "/jokes/random?firstName=";
  CUSTOM_SERVICE_URL_LAST: string = "&lastName=";

  constructor(private http: HttpClient) { }

  //TODO: call services
  public getJoke(person: Person): Observable<Joke>{
    let req = this.generateRequest(person);

    return this.http.get<Joke>(req).pipe().map(res => res['value']);
  }

  private generateRequest(person: Person) : string{
    if(person){
      return this.BASE_SERVICE_URL + this.CUSTOM_SERVICE_URL_FIRST + person.FirstName + this.CUSTOM_SERVICE_URL_LAST + person.LastName;
    }
  }


  public getRandomJokes(nr: number): Observable<Joke[]> {
    let request = 'http://api.icndb.com/jokes/random/' + nr;
    return  this.http.get(request).map(res => res['value'] );
  }

  public getRandomJokesV2(nr: number): Promise<Joke[]> {
    let request = 'http://api.icndb.com/jokes/random/' + nr;
    return  this.http.get(request).toPromise().then(data => {
        return data['value'];
    });
    
  }


  catchError(){

  }

}
