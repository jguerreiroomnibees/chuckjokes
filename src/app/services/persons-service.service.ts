import { Observable } from 'rxjs/Observable';
import { Person } from './../classes/person';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Persons } from './../mocks/mock-persons';

@Injectable()
export class PersonsServiceService {
  private Persons: Person[];

  constructor() { 
    this.Persons = Persons;
  }

  public getPersons(numOfRecords?: number): Person[] {
    if(numOfRecords){
      return this.Persons.slice(0, numOfRecords)
    }
    return this.Persons;
  }

  public getPersonByName(fName: string, lName: string): Person[] {
    return this.Persons.filter(x => x.FirstName == fName && x.LastName == lName);
  }

}
