import { Injectable } from '@angular/core';
import { Joke } from '../classes/joke';

@Injectable()
export class FavoritesServiceService {

  private favoriteJokes: Joke[];

  constructor() { }

  getFavorites(): Joke[] {
    return this.favoriteJokes;
  }

  addToFavorites(jokeToAdd: Joke) {
    this.favoriteJokes.push(jokeToAdd);
  }

  remove(id: number) {
    this.favoriteJokes = this.favoriteJokes.filter(x => x.id !== id);
  }

  //check if exists in the favorites
  checkIsFavorite(id: number): boolean {
    if(this.favoriteJokes){
      return this.favoriteJokes.find(x => x.id == id) !== undefined;
    }
    return false;
  }

}
