import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SingleJokeComponent } from './components/single-joke/single-joke.component';
import { MultiJokesComponent } from './components/multi-jokes/multi-jokes.component';

const routes: Routes = [
  //{ path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'single-joke', component: SingleJokeComponent },
  { path: 'multi-jokes', component: MultiJokesComponent },
  { path: 'multi-jokes/:nr', component: MultiJokesComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}