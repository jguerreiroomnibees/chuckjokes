import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SingleJokeComponent } from './components/single-joke/single-joke.component';
import { MultiJokesComponent } from './components/multi-jokes/multi-jokes.component';
import { FavouriteJokesComponent } from './components/favourite-jokes/favourite-jokes.component';
import { JokeComponent } from './components/joke/joke.component';
import { JokeServiceService } from './services/joke-service.service';
import { PersonsServiceService } from './services/persons-service.service'
import { FavoritesServiceService } from './services/favorites-service.service'
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './/app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    SingleJokeComponent,
    MultiJokesComponent,
    FavouriteJokesComponent,
    JokeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    JokeServiceService,
    PersonsServiceService,
    FavoritesServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
