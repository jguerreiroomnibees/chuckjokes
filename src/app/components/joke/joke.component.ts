import { FavoritesServiceService } from './../../services/favorites-service.service';
import { JokeServiceService } from './../../services/joke-service.service';
import { Component, OnInit, Input } from '@angular/core';
import { Joke } from '../../classes/joke';

@Component({
  selector: 'app-joke',
  templateUrl: './joke.component.html',
  styleUrls: ['./joke.component.css']
})
export class JokeComponent implements OnInit {
  @Input() public joke: Joke;
  public isFavorite: boolean;
  public starImageActive: string = "./../../../assets/star_active.png";
  public starImageInactive: string = "./../../../assets/star_inactive.png";

  constructor(private favoritesService: FavoritesServiceService) { }

  ngOnInit() {
    this.isFavorite = this.favoritesService.checkIsFavorite(this.joke.id);
  }

  //add of remove to favorites
  addToFavorites(){
    if(this.favoritesService.checkIsFavorite(this.joke.id)){
      this.favoritesService.addToFavorites(this.joke);
    }
    else {
      this.favoritesService.remove(this.joke.id);
    }

    this.isFavorite != this.isFavorite;
  }

}
