import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Joke } from './../../classes/joke';
import { JokeServiceService } from './../../services/joke-service.service';

@Component({
  selector: 'app-multi-jokes',
  templateUrl: './multi-jokes.component.html',
  styleUrls: ['./multi-jokes.component.css']
})
export class MultiJokesComponent implements OnInit {

  jokes: Joke[];
  jokesNr: number;

  constructor(
    private jokeService: JokeServiceService, 
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const nr = +this.route.snapshot.paramMap.get('nr');
    if(nr > 0)
      this.jokesNr = nr;
  }

  callRandomJokes(nr: number): void {
    this.jokeService.getRandomJokes(nr)
        .subscribe(jokes => {
          this.jokes = jokes
    });
  }

  callRandomJokesV1(): void {
    this.jokeService.getRandomJokes(this.jokesNr)
        .subscribe(jokes => {
          this.jokes = jokes
    });
  }

  callRandomJokesV2(nr: number): void {
    this.jokeService.getRandomJokesV2(nr)
        .then(jokes => {
          this.jokes = jokes
    });
  }
}
