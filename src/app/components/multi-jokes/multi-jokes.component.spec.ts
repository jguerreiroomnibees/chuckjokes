import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiJokesComponent } from './multi-jokes.component';

describe('MultiJokesComponent', () => {
  let component: MultiJokesComponent;
  let fixture: ComponentFixture<MultiJokesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiJokesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiJokesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
