import { Joke } from './../../classes/joke';
import { Component, OnInit, Input } from '@angular/core';

import { JokeServiceService } from './../../services/joke-service.service';
import { Person } from './../../classes/person';

@Component({
  selector: 'app-single-joke',
  templateUrl: './single-joke.component.html',
  styleUrls: ['./single-joke.component.css']
})
export class SingleJokeComponent implements OnInit {

  person: Person;
  joke: Joke;

  constructor(private jokeService: JokeServiceService) { 
    this.person = new Person();
  }

  ngOnInit() {
  }

  callJoke(): void {
      this.jokeService.getJoke(this.person)
      .subscribe(joke => {
        this.joke = new Joke(joke.id, joke.joke, joke.categories);
   } );

  }
}
