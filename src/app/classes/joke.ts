import { Category } from './category'

export class Joke {
    id: number;
    joke: string;
    categories: [string];

constructor(id: number, joke: string, categories: [string]){
    this.id = id;
    this.joke = joke;
    this.categories = categories;
    }
}


